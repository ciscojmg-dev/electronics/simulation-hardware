## Projects
### Low Pass
#### Simulation
![Low-Pass-1](/uploads/3a9d6b25a6b56184fca7e3b4c81e9f64/Low-Pass-1.m4v)
#### Images
![docs-low-pass](/uploads/e5f1ce8a438198e4bc4c40a2a21df7f6/docs-low-pass.png)
#### Docs
- En la carpeta Docs se encuentra un pdf de la documentacion del filtro pasa baja.
